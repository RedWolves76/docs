
## JAVA

[JIRA Autowatcher](https://bitbucket.org/atlassianlabs/atlassian-autowatch-plugin)  
Automatically add watchers to newly created JIRA's.

[Who's Looking](https://bitbucket.org/atlassian/whoslooking-connect/overview)  
Displays the users who are currently looking at a JIRA issue.

## Node.js
[Webhook Inspector](https://bitbucket.org/atlassianlabs/webhook-inspector)  
Inspects the response bodies of the available webhooks in Atlassian Connect.

[Entity Property Tool](https://bitbucket.org/robertmassaioli/ep-tool)  
An Atlassian Connect add-on that allows manipulation of entity properties in JIRA.

## Haskell
[My Reminders](https://bitbucket.org/atlassianlabs/my-reminders/overview)  
Sends you reminders about JIRA issues.

## Scala

[Atlas Lingo](https://bitbucket.org/atlassianlabs/atlas-lingo)  
Translate JIRA issues into other languages using Google Translate API.

[Who's Looking](https://bitbucket.org/atlassianlabs/whoslooking-connect-scala)
Displays the users who are currently looking at a JIRA issue.

## Static Add-on
Static add-ons consist of entirely static content (HTML, CSS and JavaScript) and do not have a framework.

[Who's Looking](https://bitbucket.org/atlassianlabs/atlassian-connect-whoslooking-connect-v2)  
Displays the users who are currently looking at a JIRA issue.