---
title: Extension points for project configuration
platform: cloud
product: jiracloud
category: reference
subcategory: modules 
aliases:
- /jiracloud/jira-platform-modules-project-configuration-39988366.html
- /jiracloud/jira-platform-modules-project-configuration-39988366.md
confluence_id: 39988366
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988366
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988366
date: "2016-09-15"
---
# Extension points for project configuration

This pages lists the extension points for modules on the JIRA project configuration page. 

## Project configuration menu

Adds sections and items to the menu of the project configuration screen.

#### Module type
`webSection` + `webPanel`

#### Screenshot
<img src="/cloud/jira/platform/images/jira-projectconfig-menu.png"/>

#### Sample JSON
``` json
...
"modules": {
    "webSections": [
        {
            "key": "example-menu-section",
            "location": "atl.jira.proj.config",
            "name": {
                "value": "Example add-on name"
            }
        }
    ],
    "webItems": [
        {
            "key": "example-section-link",
            "location": "example-menu-section",
            "name": {
                "value": "Example add-on link"
            }
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: For the `webSection`, set the location to `atl.jira.proj.config`. For each `webPanel`, set the location to the key of the `webSection`.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

----

## Project summary area

Adds panels to the project summary area.

#### Module type
`webPanel`

#### Screenshot
<img src="/cloud/jira/platform/images/jira-projectconfig-summary.png"/>

#### Sample JSON
``` json
...
"modules": {
    "webPanels": [
        {
            "key": "example-panel",
            "location": "webpanels.admin.summary.left-panels",
            "name": {
                "value": "Example panel"
            }
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: Set the location to `webpanels.admin.summary.left-panels` or `webpanels.admin.summary.right-panels`.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

  [i18n property]: /cloud/jira/platform/connect/modules/i18n-property
  [additional context]: /cloud/jira/platform/context-parameters