---
title: JIRA Cloud platform REST API scopes
platform: cloud
product: jiracloud
category: reference
subcategory: api
aliases:
- /jiracloud/jira-rest-api-scopes.html
- /jiracloud/jira-rest-api-scopes.md
date: "2017-10-04"
type: scopes
scopeskey: jira
---

# JIRA Cloud platform REST API scopes

{{< include path="docs/content/cloud/connect/reference/product-api-scopes.snippet.md" >}}