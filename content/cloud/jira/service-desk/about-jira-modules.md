---
title: About JIRA modules
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
date: "2016-10-31"
---

{{< reuse-page path="docs/content/cloud/jira/platform/about-jira-modules.md">}}

## JIRA Service Desk modules

-   [Agent view]
-   [Customer portal]
-   [Request create property panel]
-   [Automation action]

  [Agent view]: /cloud/jira/service-desk/agent-view
  [Customer portal]: /cloud/jira/service-desk/customer-portal
  [Request create property panel]: /cloud/jira/service-desk/request-create-property-panel
  [Automation action]: /cloud/jira/service-desk/automation-action